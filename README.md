# To Run follow the commands

    docker build -t demoincrement . # it will create the image from Dockerfile

    docker run container_incrementor # it will run the image

    docker run --env DATA_PATH=/data/filename.txt --mount type=volume,src=increment-data,target=/data imageID # for running a container with named volume concept
    
    (OR)

    docker run --env DATA_PATH=/data/number.txt -v incdata:/data imageID # for running a container with simple named volume option

    (OR)

    docker run --env DATA_PATH=/data/number.txt -v $(pwd)/files:/data imageID # for running a container with simple volume option for the current working directory 





